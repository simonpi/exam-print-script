\documentclass[a4paper]{article}
\usepackage[MnSymbol]{mathspec}
\usepackage{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage[left=1.5cm, right=1.5cm, bottom=2.3cm, top=2.3cm]{geometry}
\setmonofont[Ligatures=TeX, BoldFont={Inconsolata-Bold}]{Inconsolata-Regular}
\usepackage{color}
\usepackage{graphicx}
\usepackage{relsize}
\usepackage{listings}
\usepackage{lstlinebgrd}

\definecolor{gray}{gray}{0.55}
\pagestyle{fancy}

\fancyfoot[C]{\thepage / \pageref{LastPage}}
\fancyhead[L]{IDENTIFIER}
\fancyhead[R]{IPADDRESS}


% http://tex.stackexchange.com/questions/80113/hide-section-numbers-but-keep-numbering
\renewcommand{\thesection}{}
\renewcommand{\thesubsection}{\arabic{section}.\arabic{subsection}}
\makeatletter
\def\@seccntformat#1{\csname #1ignore\expandafter\endcsname\csname the#1\endcsname\quad}
\let\sectionignore\@gobbletwo
\let\latex@numberline\numberline
\def\numberline#1{\if\relax#1\relax\else\latex@numberline{#1}\fi}
\makeatother
\parindent 0cm

% Define appearance of Python code
\lstdefinestyle{PY}{
  language=python,
  basicstyle={\ttfamily \small},
  mathescape=true,
  basewidth=0.58em,
  columns=fixed,
  tabsize=2,
  fontadjust=true,
  frame=lb,
  xleftmargin=4.2pt,
  numbers=left,
  stepnumber=1,
  breaklines=false,
  breakindent=0pt,
  prebreak=\mbox{\tiny$\searrow$},
  postbreak=\mbox{{\color{gray}$\cdots$}},
  keywordstyle=\bfseries,
  keywords={and, as, assert, break, class, continue, def, del,
    elif, else, except, exec, finally, for, from, global,
    if, import, in, is, lambda, not, or, pass, print, raise,
    return, try, while, with, yield, True, False, None},
  numberstyle=\color{gray},
  commentstyle=\color{gray},
  stringstyle=\textit,
  showstringspaces=false,
}

% Define appearance of C++ code
\lstdefinestyle{CPP}{
  language=C++,
  mathescape=false,
  alsoletter={\\,/,*},
  morekeywords={\\brief,\\return,\\param, Dune, Eigen, MatrixXd, VectorXd, assert, Zero},
  literate={\\f}{}0,
  basicstyle={\ttfamily \small },
}

\lstdefinestyle{MATLAB}{
  language=Matlab,
  mathescape=false,
  basewidth=0.58em,
  basicstyle={\ttfamily \small},
  breakindent=0pt,
  breaklines=true,
  columns=fixed,
  commentstyle=\color{gray},
  fontadjust=true,
  frame=lb,
  keywords={function, while, for, break, end, properties, events},
  keywordstyle=\bfseries,
  numbers=left,
  numberstyle=\color{gray},
  postbreak=\mbox{{\color{gray}$\cdots$}},
  postbreak=\mbox{{\color{gray}$\cdots$}},
  prebreak=\mbox{\tiny$\searrow$},
  prebreak=\mbox{\tiny$\searrow$},
  showstringspaces=false,
  stepnumber=2,
  stepnumber=2,
  stringstyle=\color{gray},
  tabsize=2,
  xleftmargin=4.2pt,
}


\lstset{literate={\\f}{}0}
\newcommand{\f}{} % Escape C++ doxygen LaTeX commands of the form \f$ fancy math \f$

\date{}
\title{IDENTIFIER \\ IPADDRESS }
\author{LECTURETITLE}

\begin{document}

\maketitle
\tableofcontents
