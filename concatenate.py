#!/usr/bin/env python

import copy, os, sys
from pyPdf import PdfFileWriter, PdfFileReader
from subprocess import call

if len(sys.argv) < 2:

    print('Please pass path to results')

else:

    for (dirpath, subdirs, filenames) in os.walk(sys.argv[1]):
        for filename in filenames:
            if filename.lower().endswith('.pdf'):
                path = os.path.join(dirpath, filename)
                call([
                    'pdfnup', '--nup', '2x1', path, '-o',
                    path[:-4] + '-nup.pdf'
                ])

    output = PdfFileWriter()
    output_page_number = 0
    alignment = 2  # to align on even pages
    for (dirpath, subdirs, filenames) in os.walk(sys.argv[1]):
        for filename in filenames:
            if filename.lower().endswith('nup.pdf'):
                # This code is executed for every file in turn
                path = os.path.join(dirpath, filename)
                input = PdfFileReader(open(path))
                for p in [
                        input.getPage(i)
                        for i in range(0, input.getNumPages())
                ]:
                    # This code is executed for every input page in turn
                    output.addPage(p)
                    output_page_number += 1
                while output_page_number % alignment != 0:
                    output.addBlankPage()
                    output_page_number += 1

    path = os.path.join(sys.argv[1], 'out.pdf')
    output.write(open(path, 'w'))

    pdf_in = open(path, 'r')
    pdf_reader = PdfFileReader(pdf_in)
    pdf_writer = PdfFileWriter()

    for pagenum in range(pdf_reader.numPages):
        page = pdf_reader.getPage(pagenum)
        if pagenum % 2:
            page.rotateClockwise(180)
        pdf_writer.addPage(page)

    path = os.path.join(sys.argv[1], 'out_rotated.pdf')
    pdf_writer.write(open(path, 'w'))
