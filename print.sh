#!/bin/zsh -f

if [[-z ${EXAM_PRINT_OPTIONS}]];
then
    export EXAM_PRINT_OPTIONS="-o sides=two-sided-long-edge -o landscape -o fit-to-page -o media=A4 -o number-up=2 -o number-up-layout=lr"
else
    echo "Using printer options from env. variable,  EXAM_PRINT_OPTIONS: ${EXAM_PRINT_OPTIONS}"
fi

### NO CHANGES BELOW THIS LINE NECESSARY ###
zparseopts -D -E -A myoptions -help -printer:=oap

show_help() {
    echo "Usage: $1 [--printer name] /path/to/results"
    echo "Hint: use \`lpstat -p\` to list available printers."
    echo
    echo "Print options: ${EXAM_PRINT_OPTIONS}"
    echo "\n"
}

if (( $+myoptions[--help] )); then
    show_help $0
    exit 0
fi

EXAM_SETUP_USER=.exam-setup-user

if [[ $# -eq 0 ]]; then
    echo "*ERROR* not enought arguments.\n"
    show_help $0
    exit 1
fi

printer=${oap#--printer}
if [[ $printer == "" ]]; then
    echo "using printer: p-hg-j-42"
    printer=p-hg-j-42
fi

# send to printer
lpstat -p | grep "${printer}" > /dev/null
if [[ $? -eq 0 ]]; then
    echo "using lpr options: ${EXAM_PRINT_OPTIONS}"
    for res_dir in $@; do
        (
            cd $res_dir
            # get all subfolders which contain the EXAM_SETUP_USER file
            results_dir=$(print -l **/${EXAM_SETUP_USER}(:h))
            for ldir in ${(f)results_dir}; do
                tail=${ldir:t}
                pdfpath=${tail}.pdf
                if [[ -f "$pdfpath" ]]; then
                    echo "\t $pdfpath"
                    lpr -P ${printer} ${EXAM_PRINT_OPTIONS} $pdfpath
                fi
            done
        )
    done
else
    echo "*ERROR* cannot find printer: ${printer}"
    echo "=> Hint: use \`lpstat -p\` to list available printers."
fi
